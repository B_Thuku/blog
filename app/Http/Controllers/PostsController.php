<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    //

    public function index(){
        //fetch all posts

        $posts=Post::latest()->get();

        return view('posts.index',compact('posts'));
    }


    public function show(Post $post){

        return view('posts.show',compact('post'));

    }
    public function create(){

        return view('posts.create');

    }

    public function store(){

        //validate before posting
        $this->validate(request(),[
            'title'=>'required',
            'body'=>'required'

        ]);

        //create a new post using request data


        Post::create([

            'title'=>request('title'),
            'body'=>request('body')

        ]);

        //redirect to the home page

        return redirect('/');

    }
}
